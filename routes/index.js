const express = require('express');
const router = express.Router();
const { ensureAuthenticated } = require('../config/auth');

// Route for the Home Page
router.get('/', (req, res) => {
    res.render('welcome');
})

// Route for the Dashboard
router.get('/dashboard', ensureAuthenticated, (req, res) => {
    res.render('dashboard', {
        name: req.user.name
    });
})

module.exports = router;